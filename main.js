/**
 * Created by Leonard Cheong on 31/3/2016.
 */

//Load express
var express = require("express");

//Create an application
var app = express();

//Define my document root
// all static files serve from given directory
app.use(
    express.static(__dirname + "/public")
);

app.listen(3000, function() {
    console.info("Application is listening on port 3000");
});


